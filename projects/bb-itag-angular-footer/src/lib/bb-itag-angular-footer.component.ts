import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ThemePalette } from '@angular/material/core';
import { faExclamationTriangle, faNetworkWired } from '@fortawesome/free-solid-svg-icons';
import { IFooterConfiguration } from './models/IFooterConfiguration';

@Component({
  selector: 'lib-bb-itag-angular-footer',
  templateUrl: 'bb-itag-angular-footer.component.html',
  styleUrls: [ 'bb-itag-angular-footer.component.css' ]
})
export class BbItagAngularFooterComponent {

  // declare all the defaults
  public internalFooterConfiguration: IFooterConfiguration = {
    appVersion: null,
    onlineThemePalette: 'primary',
    offlineThemePalette: 'warn',
    onlineFaIconDefinition: faNetworkWired,
    offlineFaIconDefinition: faExclamationTriangle,
    onlineText: 'ONLINE',
    offlineText: 'OFFLINE',
    showPointer: true,
    versionRouterLink: null,
    versionRouterLinkClass: 'defaultVersion'
  };

  @Input()
  public set appVersion(value: string) {
    this.internalFooterConfiguration.appVersion = value;
    this.updateStatus();
  }
  @Input()
  public set showPointer(value: boolean) {
    this.internalFooterConfiguration.showPointer = value;
    this.updateStatus();
  }
  @Input()
  public set onlineThemePalette(value: ThemePalette) {
    this.internalFooterConfiguration.onlineThemePalette = value;
    this.updateStatus();
  }
  @Input()
  public set offlineThemePalette(value: ThemePalette) {
    this.internalFooterConfiguration.offlineThemePalette = value;
    this.updateStatus();
  }
  @Input()
  public set onlineFaIconDefinition(value: any) {
    this.internalFooterConfiguration.onlineFaIconDefinition = value;
    this.updateStatus();
  }
  @Input()
  public set offlineFaIconDefinition(value: any) {
    this.internalFooterConfiguration.offlineFaIconDefinition = value;
    this.updateStatus();
  }
  @Input()
  public set onlineText(value: string) {
    this.internalFooterConfiguration.onlineText = value;
    this.updateStatus();
  }
  @Input()
  public set offlineText(value: string) {
    this.internalFooterConfiguration.offlineText = value;
    this.updateStatus();
  }
  @Input()
  public set versionRouterLink(value: string) {
    this.internalFooterConfiguration.versionRouterLink = value;
    this.updateStatus();
  }
  @Input()
  public set versionRouterLinkClass(value: string) {
    this.internalFooterConfiguration.versionRouterLinkClass = value;
    this.updateStatus();
  }
  @Output()
  public appVersionClicked: EventEmitter<string> = new EventEmitter<string>();

  @Input()
  public set online(online: boolean) {
    if ( online === null) {
      // if we get a null as value, we using the default value true
      online = true;
    }
    this.isInternalOnline = online;
    this.updateStatus();
  }

  constructor(
  ) {
    this.updateStatus();
  }

  public isInternalOnline = true;
  public statusText = this.internalFooterConfiguration.onlineText;
  public statusIcon: any = this.internalFooterConfiguration.onlineFaIconDefinition;
  public backgroundTheme: ThemePalette = this.internalFooterConfiguration.onlineThemePalette;

  private updateStatus() {
    if ( this.isInternalOnline ) {
      this.statusText = this.internalFooterConfiguration.onlineText;
      this.statusIcon = this.internalFooterConfiguration.onlineFaIconDefinition;
      this.backgroundTheme = this.internalFooterConfiguration.onlineThemePalette;
    } else {
      this.statusText = this.internalFooterConfiguration.offlineText;
      this.statusIcon = this.internalFooterConfiguration.offlineFaIconDefinition;
      this.backgroundTheme = this.internalFooterConfiguration.offlineThemePalette;
    }
  }

  public onAppVersionClick() {
    if (this.internalFooterConfiguration.showPointer) {
      this.appVersionClicked.emit(this.internalFooterConfiguration.appVersion);
    }
  }
}

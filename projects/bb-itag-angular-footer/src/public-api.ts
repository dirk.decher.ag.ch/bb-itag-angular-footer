/*
 * Public API Surface of bb-itag-angular-footer
 */

export * from './lib/bb-itag-angular-footer.component';
export * from './lib/bb-itag-angular-footer.module';
export { IFooterConfiguration } from './lib/models/IFooterConfiguration';

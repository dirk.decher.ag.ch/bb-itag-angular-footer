import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-demo',
  templateUrl: './demo.component.html',
})
export class DemoComponent  {

  constructor(
    public activeRouter: ActivatedRoute
  ) { }

}
